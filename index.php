<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        $cajas=[
            "nombre"=>[
                "id"=>"inombre",
                "class"=>"clase1",
                "style"=>[
                    "blackground-color"=>"gray",
                    "color"=>"white",
                ],
                "value"=>"Mi nombre",
                "placeholder"=>"Escribe tu nombre",
                "type"=>"text",
            ],
            "edad"=>[
                "type"=>"number",
                "placeholder"=>"Escribe tu edad",
            ],
            "poblacion"=>[
            ]
        ];
        
        function dibujarCajas($datos){
            $salida=[];
            foreach ($datos as $nombre => $opciones) {
                $defecto=[
                    "id"=>"i".$nombre,
                    "placeholder"=>"escribe tu " . $nombre,
                    "type"=>"text",
                    "name"=>$nombre,
                ];
                $salida[$nombre ]=dibujarCaja(array_merge($defecto,$opciones));
            }
            return $salida;
        }
        
        function dibujarCaja($datos){
            
            $salida='<label for="'. $datos["id"] .'">'. $datos["name"] .'</label>';
            $salida.='<input ';
            foreach ($datos as $atributo => $valor) {
                if(is_array($valor)){
                    $todo='';
                    foreach ($valor as $k => $v) {
                        $todo.=$k . ':' . $v . ';';
                    }
                    $valor=$todo;
                }
                $salida.= $atributo . '="' . $valor . '" ';
            }
            $salida.=">";
            return $salida;
        }
        
        $controles=dibujarCajas($cajas);
        var_dump($controles);
        ?>
    </head>
    <body>
        <h1>Ejemplo de formulario dinamico</h1>
        <form name="unico">
            <div>
                <?= $controles["nombre"] ?>
            </div>
            <div>
                <?= $controles["edad"] ?>
            </div>
            <div>
                <?= $controles["poblacion"] ?>
            </div>
            
            
            
            <button>Enviar</button>
        </form>
        
    </body>
</html>
